<?php 

//Add ACF Options pages
if (function_exists('acf_add_options_sub_page'))
{
    acf_add_options_sub_page('Header Options');
    acf_add_options_sub_page('Home page Options');
}
function home_background(){
	
	$bg_img = get_field('hm_image_background', 'option');
	if($bg_img){
		$bg_img = 'style="background-image: url('.$bg_img.');"';
	}

	
	return $bg_img;
}