<?php 
/**
 * Template name: Front page
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="description-block">
				<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						// Include the page content template.
						the_content();

					// End the loop.
					endwhile;
					
					$btn_title = get_field('button_title_description_box', 'option');
					$btn_link = get_field('button_link_description_box', 'option');
					
					if($btn_title && $btn_title){
						echo '<a class="btn" href="#modal" data-toggle="modal">'.$btn_title.'</a>';
					}
					
				?>
				
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
