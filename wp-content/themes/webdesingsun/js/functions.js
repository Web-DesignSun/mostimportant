/**
 * Theme functions file.
 *
 * global value wds
 * - retina ( is retina device )
 * - mobile ( is mobile device )
 *
 */

( function( $ ) {
	console.log("is retina: "+wds.retina);
	console.log("is mobile: "+wds.mobile);
	

	$( ".modal" ).on('shown', function(){
		$('body').removeClass('modal-open');
		$('html').addClass('modal-open');
		$('.description-block').hide();
		$('.passcode-box, .wrap-video').removeClass('hidden');
		$('#passcode').val('');
	});
	/*
	$( "#modal-video" ).on('shown', function(){
		$('#modal-form').modal('toggle');
		$('body').removeClass('modal-open');
		$('html').addClass('modal-open');
	});
	$( "#modal-feedback-1" ).on('shown', function(){
		$('#modal-video').modal('toggle');
		$('body').removeClass('modal-open');
		$('html').addClass('modal-open');
	});
	*/
	$('.modal').on('hidden', function () {
		$('html').removeClass('modal-open');
		$('.description-block').show();	
		$('.modal-wrap').addClass('hidden');
		$('#modal-form').removeClass('hidden');
	})
	
	$('body').on('click', '.btn-modal', function(){
		var dataModal = $(this).attr('data-modal');
		
		
		if(dataModal == '#modal-video'){
			var $passCode = $('#passcode').val();
			if($passCode == 007){
				$('.passcode-box, .wrap-video').addClass('hidden');
				$('.error.invalid, .no-error').addClass('hidden');
				///
				$('.modal-wrap').addClass('hidden');
				$(dataModal).removeClass('hidden');
			}else{
				$('.no-error').addClass('hidden');
				$('.error.invalid').removeClass('hidden');
				
			}
		
		}else{
			$('.modal-wrap').addClass('hidden');
			$(dataModal).removeClass('hidden');
		}
	});
	
	//feedback-1
	$(".rating input:radio").attr("checked", false);

    $('.rating input').click(function () {
        $(".rating span").removeClass('checked');
        $(this).parent().addClass('checked');
    });

    $('input:radio').change(
      function(){
        var userRating = this.value;
        console.log(userRating);
    }); 
	//End feedback-2
	
	//feedback-2
	$(".radio-two").change(function () {
		var $this = $(this);
			console.log($this.val());
		if($this.val() == 'yes'){
			$('.fields-row').show();
		}else if($this.val() == 'no'){
			$('.fields-row').hide();
			$('.field-item').each(function(){
				$(this).val('')
			});
		}
	});
	
	
	$('.add-fields').click(function(){
		var $clone = $('.clone-field.hidden').clone();
		
		$('.box-add').before($clone);
		$('.fields-row .clone-field').removeClass('hidden');
		
	});
	
	$('body').on('click', '.remove-fields', function(){
		$(this).parents('.clone-field').remove();
	});
	
	$('body').on('click', '.dropdown-toggle', function(){
		$(this).parent('.dropdown').addClass('open');
	});
	
	$('body').on('click', '.dropdown-menu li', function(){
		var $this = $(this);
		
		if($this.hasClass('get')){
			$val = $this.text();
			$this.parents('.dropdown').removeClass('open');
		}else if($this.hasClass('get-val')){
			$val = $this.parents('.dropdown').find('.other').val();
		
			if($val == ''){
				return;
			}
		}
		$this.parents('.dropdown').children('.field-relationship').val($val);
		$this.parents('.dropdown').children('.dropdown-toggle').text($val)
	//	$('.dropdown-toggle b').remove().appendTo($('.dropdown-toggle').text($val));
		
	});
	
	
	$('body').on('input', '.other', function(){
		
		var $val = $(this).val();
		var $field = $(this).parents('.dropdown').children('.dropdown-toggle');
		
		$field.text($val);
		$(this).parents('.dropdown').children('.field-relationship').val($val);
	});
	
	
	$(document).click(function(event) { 
		if(!$(event.target).closest('.dropdown').length) {
			if($('.dropdown').is(":visible")) {
				$('.dropdown').removeClass('open');
			}
		}        
	});
	//End feedback-2
} )( jQuery );
