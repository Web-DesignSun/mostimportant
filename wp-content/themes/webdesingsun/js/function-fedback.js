( function( $ ) {
	
	$(".radio-two").change(function () {
		var $this = $(this);
			console.log($this.val());
		if($this.val() == 'yes'){
			$('.fields-row').show();
		}else if($this.val() == 'no'){
			$('.fields-row').hide();
			$('.field-item').each(function(){
				$(this).val('')
			});
		}
	});
	
	
	$('.add-fields').click(function(){
		var $clone = $('.clone-field.hidden').clone();
		
		$('.box-add').before($clone);
		$('.fields-row .clone-field').removeClass('hidden');
		
	});
	
	$('body').on('click', '.remove-fields', function(){
		$(this).parents('.clone-field').remove();
	});
	
	$('body').on('click', '.dropdown-toggle', function(){
		$(this).parent('.dropdown').addClass('open');
	});
	
	$('body').on('click', '.dropdown-menu li', function(){
		var $this = $(this);
		
		if($this.hasClass('get')){
			$val = $this.text();
			$this.parents('.dropdown').removeClass('open');
		}else if($this.hasClass('get-val')){
			$val = $this.parents('.dropdown').children('.other').val();
			
			if($val == ''){
				return;
			}
		}
		
		$this.parents('.dropdown').children('.field-relationship').val($val);
		$this.parents('.dropdown').children('.dropdown-toggle').text($val)
	//	$('.dropdown-toggle b').remove().appendTo($('.dropdown-toggle').text($val));
		
	});
	
	
	$('body').on('input', '.other', function(){
		
		var $val = $(this).val();
		var $field = $(this).parents('.dropdown').children('.dropdown-toggle');
		
		$field.text($val);
		$(this).parents('.dropdown').children('.field-relationship').val($val);
	});
	
	
	$(document).click(function(event) { 
		if(!$(event.target).closest('.dropdown').length) {
			if($('.dropdown').is(":visible")) {
				$('.dropdown').removeClass('open');
			}
		}        
	});
	
} )( jQuery );
