	</div><!-- .site-content -->

</div><!-- .site -->
<?php if(is_front_page()): ?>
<div id="modal" class="modal fade" tabindex="-1">
	<!--Wrap Modal 1 -->
	<div id="modal-form" class="modal-wrap form">
		<div class="modal-header">
			<a  class="close" href="#" data-dismiss="modal" aria-hidden="true"></a>
			<h3>To watch this private presentation, please fill out the fields below.</h3>
		</div>
		<div class="modal-body">
			<div class="field-box">
				<p>First Name</p>
				<input type="text" id="first-name" />
			</div>
			<div class="field-box">
				<p>last name</p>
				<input type="text" id="last-name" />
			</div>
			<div class="field-box">
				<p>Email</p>
				<input type="email" id="email" />
			</div>
			<div class="field-box">
				<p>ZIP CODE</p>
				<input type="text" id="zip-code" />
			</div>
			<div class="field-box">
				<p>passcode <span class="required">*</span></p>
				<input type="text" id="passcode" />
			</div>
		</div>
		<div class="modal-footer">
			<p class="no-error"><span class="required">*</span> The person who invited you should have given you a passcode. If not, please ask them for one. If no one invited you, click here.</p>
			<p class="error expired hidden">The passcode you entered has expired. Please contact the person who invited you to request a new passcode.</p>
			<p class="error invalid hidden">The passcode you entered is invalid. Please contact the person who invited you, or <a href="#">click here</a> if you never received a valid passcode.</p>
		</div>
		<div class="btn-box">
			<a id="sumbut-form" class="btn btn-modal" data-modal="#modal-video"><span class="icon-play"></span>watch the PRESENTATION</a>
		</div>
	</div>
	<!--Wrap Modal 2 -->
	<div id="modal-video" class="modal-wrap video hidden">
		<a  class="close" href="#" data-dismiss="modal" aria-hidden="true"></a>
		<div class="modal-body">
			
			<!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/andy.png" alt="" />-->
			<?php the_field('video', 'option'); ?>
			<!--<a class="btn btn-video" href="#" ><span class="icon-play"></span><p>Play Video</p></a>-->
			<div class="wrap-video"></div>
			<div class="passcode-box">
				<!--
				<p>Enter passcode to watch video</p>
				<div class="field-box">
					<input type="text" id="passcodevideo" />
				</div>
				<div class="btn-box">
					<a id="sumbutpasscodevideo" class="btn"><span class="icon-play"></span>sumbit</a>
				</div>
				-->
				<div class="error-passcode hidden">Sorry, you need a valid passcode to watch the presentation.</div>
			</div>
		</div>
		<div class="modal-footer">
			<h1>The Most Important Thing</h1>
			<p>To download the ActivInsight worksheet and follow along with the presentation, click the button below.</p>
			<p>This is optional, but will make it more interactive and more valuable.</p>
		</div>
		<div class="btn-box">
			<a id="download-worksheet" class="btn transparent" href="<?php bloginfo('stylesheet_directory'); ?>/file/-ActivInsightWorksheet.pdf" download>download worksheet</a>
			<a class="next-link nav-modal-link btn-modal" href="#"  data-modal="#modal-feedback-1">next</a>
		</div>
	</div>
	<!--Wrap Modal 3 -->
	<div id="modal-feedback-1" class="modal-wrap feedback-1 hidden">
		<div class="container-form">
			<div class="form-header">
				<a  class="close" href="#" data-dismiss="modal" aria-hidden="true"></a>
				<h1>“The Most Important Thing” Feedback</h1>
			</div>
			<div class="form-body">
				<div class="table-form">
					<div class="row-tb">
						<div class="col-tb-1">
							<p>Please rate the presentation:</p>
						</div>
						<div class="col-tb-2">
							 <div class="rating">
								<span><input type="radio" name="rating" id="str5" value="5"><label for="str5"><i class="fa fa-star" aria-hidden="true"></i></label></span>
								<span><input type="radio" name="rating" id="str4" value="4"><label for="str4"><i class="fa fa-star" aria-hidden="true"></i></label></span>
								<span><input type="radio" name="rating" id="str3" value="3"><label for="str3"><i class="fa fa-star" aria-hidden="true"></i></label></span>
								<span><input type="radio" name="rating" id="str2" value="2"><label for="str2"><i class="fa fa-star" aria-hidden="true"></i></label></span>
								<span><input type="radio" name="rating" id="str1" value="1"><label for="str1"><i class="fa fa-star" aria-hidden="true"></i></label></span>
							</div>
						</div>
					</div>
					<div class="row-tb">
						<div class="col-tb-1">
							<p>What did you<br /> think of the<br /> presentation?</p>
						</div>
						<div class="col-tb-2">
							<textarea class="textarea-field"></textarea>
						</div>
					</div>
					<div class="row-tb">
						<div class="col-tb-1">
							<p>Do you have<br /> any questions for<br /> Andrew Bernstein<br /> (the presenter)?<br /> (These will be sent<br /> directly to him.)</p>
						</div>
						<div class="col-tb-2">
							<textarea class="textarea-field"></textarea>
						</div>
					</div>
					<div class="row-tb">
						<div class="col-tb-1">
							<p>Do you have<br /> any questions for<br /> [REFERRAL SOURCE]?<br /> (These will be sent<br /> directly to<br /> [Referral First Name].)</p>
						</div>
						<div class="col-tb-2">
							<textarea class="textarea-field"></textarea>
						</div>
					</div>
				</div>
				
			</div>
			<div class="form-footer">
				<a class="next-prev nav-modal-link btn-modal" href="#"  data-modal="#modal-video">BACK</a>
				<a id="sumbut-feedback" class="btn btn-small btn-modal" data-modal="#modal-feedback-2">CONTINUE TO NEXT STEP</a>
			</div>
		</div>
	</div>
	<!--Wrap Modal 4 -->
	<div id="modal-feedback-2" class="modal-wrap feedback-2 hidden">
		<div class="container-form">
			<div class="form-header">
				<a  class="close" href="#" data-dismiss="modal" aria-hidden="true"></a>
				<h1>“The Most Important Thing” Feedback</h1>
			</div>
			<div class="form-body">
				
				<div class="form-row">
					<p>Would you like a complimentary 1-month pass to the Resilience Academy Website<br/>so you can learn to apply this process in your life?</p>
					<div class="radio-box">	
						<input type="radio" class="radio" id="yes-1" name="selector-1" value="yes" />
						<label for="yes-1">yes</label>
					</div>
					<div class="radio-box">
						<input type="radio" class="radio" id="no-1" name="selector-1" value="no" />
						<label for="no-1">no</label>
					</div>						
				</div>
				<div class="form-row">
					<p>Would you like to share this video presentation with others?<br/> Enter their information below and they will receive an email invitation<br/> courtesy of [referral source].</p>
					<div class="radio-box">	
						<input type="radio" class="radio radio-two" id="yes-2" name="selector-2" value="yes" />
						<label for="yes-2">yes</label>
					</div>
					<div class="radio-box">
						<input type="radio" class="radio radio-two" id="no-2" name="selector-2" value="no" />
						<label for="no-2">no</label>
					</div>
				</div>
				<div class="fields-row hidden">
					<div class="prime-fields">
						<div class="col">
							<span>first name</span>
							<input type="text" id="frist-name" class="field-item" >
						</div>
						<div class="col">
							<span>last name</span>
							<input type="text" id="last-name" class="field-item" >
						</div>
						<div class="col">
							<span>email*</span>
							<input type="email" id="email" class="field-item" >
						</div>
						<div class="col last">
							<span>relationship to you</span>
							<!--
							<select id="relationship-1" class="field-item relationship" name="relationship" placeholder="PULLDOWN">
								<option value="My friend">My friend</option>
								<option value="My colleague">My colleague</option>
								<option value="My mother">My mother</option>
								<option value="My father">My father</option>
								<option value="My son">My son</option>
								<option value="My daughter">My daughter</option>
								<option value="My sibling">My sibling</option>
								<option value="customOption">Others</option>
							</select>
							<input class="custom-relationship" name="custom-relationship" style="display:none;" disabled="disabled" >
							-->		
							<ul class="dropdown-relationship">
								<li class="dropdown">
									<input type="hidden" class="field-relationship" name="field-relationship">
									<a href="#" class="dropdown-toggle"  onclick="return false">PULLDOWN<b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li class="get" >My friend</li>
										<li class="get" >My colleague</li>
										<li class="get" >My mother</li>
										<li class="get" >My father</li>
										<li class="get" >My son</li>
										<li class="get" >My daughter</li>
										<li class="get" >My sibling</li>
										<li class="get-val"><a href="#" onclick="return false"><input type="text" class="other" placeholder="Others"/></a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
					<div class="box-add">
						<p>* We will not share their email address with anyone else.</p>
						<a class="add-fields">+ add</a>
					</div>
				</div>
			</div>
			<div class="form-footer">
				<p class="hidden">Your feedback was sent successfully. Thank you for taking time to learn about the most important thing for individual and family happiness. [Referral source] will be in touch with ways to go deeper into this material.</p>
				<div class="clone-field hidden">
					<div class="col">
						<span>first name</span>
						<input type="text" id="frist-name" class="field-item" >
					</div>
					<div class="col">
						<span>last name</span>
						<input type="text" id="last-name" class="field-item" >
					</div>
					<div class="col">
						<span>email*</span>
						<input type="email" id="email" class="field-item" >
					</div>
					<div class="col last">
						<span>relationship to you</span>
						<ul class="dropdown-relationship">
							<li class="dropdown">
								<input type="hidden" class="field-relationship" name="field-relationship">
								<a href="#" class="dropdown-toggle"  onclick="return false">PULLDOWN<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li class="get" >My friend</li>
									<li class="get" >My colleague</li>
									<li class="get" >My mother</li>
									<li class="get" >My father</li>
									<li class="get" >My son</li>
									<li class="get" >My daughter</li>
									<li class="get" >My sibling</li>
									<li class="get-val"><a href="#" onclick="return false"><input type="text" class="other" placeholder="Others"/></a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="remove">
						<a class="remove-fields"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
					</div>
				</div>
				<a class="next-prev nav-modal-link btn-modal" href="#"  data-modal="#modal-feedback-1">BACK</a>
				<a id="sumbut-feedback" class="btn btn-small" data-toggle="modal">SUBMIT</a>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php wp_footer(); ?>

</body>
</html>
