<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
if ( ! function_exists( 'wds_setup' ) ) :
	function wds_setup() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Let WordPress manage the document title.
		add_theme_support( 'title-tag' );

		// Enable support for Post Thumbnails on posts and pages.
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 825, 510, true );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'webdesingsun' )
		) );

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		) );

		// Enable support for Post Formats.
		add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
		) );
		
		// Add thumbnail size 
		
		/* 
			add_image_size( 'desktop', 1920 ); 
			add_image_size( 'tablet', 900 ); 
			add_image_size( 'mobile', 640 ); 
		*/
	}
endif; 
add_action( 'after_setup_theme', 'wds_setup' );

/**
 * Register widget area.
 */
function wds_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'webdesingsun' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'webdesingsun' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wds_widgets_init' );

/**
 * JavaScript Detection.
 */
function wds_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'wds_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 */
function wds_scripts() {

	// Load our main stylesheet.
	wp_enqueue_style( 'webdesingsun-style', get_stylesheet_uri() );
	wp_enqueue_style( 'webdesingsun-fonts', get_template_directory_uri() . '/css/fonts.css' );
	wp_enqueue_style( 'webdesingsun-bootstrap-modal', get_template_directory_uri() . '/css/bootstrap-modal.css' );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	wp_enqueue_script( 'bootstrap-modalmanager-script', get_template_directory_uri() . '/js/bootstrap-modalmanager.js', array( 'jquery' ), '20150330', true );
	wp_enqueue_script( 'bootstrap-modal-script', get_template_directory_uri() . '/js/bootstrap-modal.js', array( 'jquery' ), '20150330', true );
	
	wp_enqueue_script( 'webdesingsun-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	
	if(is_page_template('template/template-feedback.php')){
		wp_enqueue_script( 'function-fedback-script', get_template_directory_uri() . '/js/function-fedback.js', array( 'jquery' ), '20150330', true );
	}
	
	if(is_page_template('template/template-most-important-thing-feedback.php')){
		wp_enqueue_script( 'function-fedback-script', get_template_directory_uri() . '/js/function-fedback-2.js', array( 'jquery' ), '20150330', true );
	}
	
	wds_viewport();
}
add_action( 'wp_enqueue_scripts', 'wds_scripts' );


// Determining the type of device
function wds_viewport() {
	require_once 'inc/mobile_detect.php';

	global $wds;
	$detect = new Mobile_Detect;
	
	// check device
	if( $detect->isTablet() ){
		$wds['device'] = 'tablet';
	} else if ( $detect->isMobile() ) {
		$wds['device'] = 'mobile';
	} else {
		$wds['device'] = 'desktop';
	}
	
	// set image size
	$wds['image-size'] = $wds['device'];

	// check is retina device
	$wds['retina'] = ( $detect->is('iOS') ) ? true : false;
	
	wp_localize_script( 'webdesingsun-script', 'wds', array(
		'retina' => $wds['retina'],
		'mobile' => ( $wds['device'] == "mobile" ) ? true : false,
	) );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/functions-theme.php';
