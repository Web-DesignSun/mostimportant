<?php 
/**
 * Template name: Feedback-2
 */

get_header(); ?>
	<div class="wrap-form"></div>
	<div id="primary" class="content-area feedback">
		<main id="main" class="site-main" role="main">
			<div class="container-form">
				<div class="form-header">
					<a  class="close" href="#" data-dismiss="modal" aria-hidden="true"></a>
					<h1>“The Most Important Thing” Feedback</h1>
				</div>
				<div class="form-body">
					<div class="table-form">
						<div class="row-tb">
							<div class="col-tb-1">
								<p>Please rate the presentation:</p>
							</div>
							<div class="col-tb-2">
								 <div class="rating">
									<span><input type="radio" name="rating" id="str5" value="5"><label for="str5"><i class="fa fa-star" aria-hidden="true"></i></label></span>
									<span><input type="radio" name="rating" id="str4" value="4"><label for="str4"><i class="fa fa-star" aria-hidden="true"></i></label></span>
									<span><input type="radio" name="rating" id="str3" value="3"><label for="str3"><i class="fa fa-star" aria-hidden="true"></i></label></span>
									<span><input type="radio" name="rating" id="str2" value="2"><label for="str2"><i class="fa fa-star" aria-hidden="true"></i></label></span>
									<span><input type="radio" name="rating" id="str1" value="1"><label for="str1"><i class="fa fa-star" aria-hidden="true"></i></label></span>
								</div>
							</div>
						</div>
						<div class="row-tb">
							<div class="col-tb-1">
								<p>What did you<br /> think of the<br /> presentation?</p>
							</div>
							<div class="col-tb-2">
								<textarea class="textarea-field"></textarea>
							</div>
						</div>
						<div class="row-tb">
							<div class="col-tb-1">
								<p>Do you have<br /> any questions for<br /> Andrew Bernstein<br /> (the presenter)?<br /> (These will be sent<br /> directly to him.)</p>
							</div>
							<div class="col-tb-2">
								<textarea class="textarea-field"></textarea>
							</div>
						</div>
						<div class="row-tb">
							<div class="col-tb-1">
								<p>Do you have<br /> any questions for<br /> [REFERRAL SOURCE]?<br /> (These will be sent<br /> directly to<br /> [Referral First Name].)</p>
							</div>
							<div class="col-tb-2">
								<textarea class="textarea-field"></textarea>
							</div>
						</div>
					</div>
					
				</div>
				<div class="form-footer">
					
					<a id="sumbut-feedback" class="btn btn-small" data-toggle="modal">CONTINUE TO NEXT STEP</a>
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
