<?php 
/**
 * Template name: Feedback
 */

get_header(); ?>
	<div class="wrap-form"></div>
	<div id="primary" class="content-area feedback">
		<main id="main" class="site-main" role="main">
			<div class="container-form">
				<div class="form-header">
					<a  class="close" href="#" data-dismiss="modal" aria-hidden="true"></a>
					<h1>“The Most Important Thing” Feedback</h1>
				</div>
				<div class="form-body">
					
					<div class="form-row">
						<p>Would you like a complimentary 1-month pass to the Resilience Academy Website<br/>so you can learn to apply this process in your life?</p>
						<div class="radio-box">	
							<input type="radio" class="radio" id="yes-1" name="selector-1" value="yes" />
							<label for="yes-1">yes</label>
						</div>
						<div class="radio-box">
							<input type="radio" class="radio" id="no-1" name="selector-1" value="no" />
							<label for="no-1">no</label>
						</div>						
					</div>
					<div class="form-row">
						<p>Would you like to share this video presentation with others?<br/> Enter their information below and they will receive an email invitation<br/> courtesy of [referral source].</p>
						<div class="radio-box">	
							<input type="radio" class="radio radio-two" id="yes-2" name="selector-2" value="yes" />
							<label for="yes-2">yes</label>
						</div>
						<div class="radio-box">
							<input type="radio" class="radio radio-two" id="no-2" name="selector-2" value="no" />
							<label for="no-2">no</label>
						</div>
					</div>
					<div class="fields-row hidden">
						<div class="prime-fields">
							<div class="col">
								<span>first name</span>
								<input type="text" id="frist-name" class="field-item" >
							</div>
							<div class="col">
								<span>last name</span>
								<input type="text" id="last-name" class="field-item" >
							</div>
							<div class="col">
								<span>email*</span>
								<input type="email" id="email" class="field-item" >
							</div>
							<div class="col last">
								<span>relationship to you</span>
								<!--
								<select id="relationship-1" class="field-item relationship" name="relationship" placeholder="PULLDOWN">
									<option value="My friend">My friend</option>
									<option value="My colleague">My colleague</option>
									<option value="My mother">My mother</option>
									<option value="My father">My father</option>
									<option value="My son">My son</option>
									<option value="My daughter">My daughter</option>
									<option value="My sibling">My sibling</option>
									<option value="customOption">Others</option>
								</select>
								<input class="custom-relationship" name="custom-relationship" style="display:none;" disabled="disabled" >
								-->		
								<ul class="dropdown-relationship">
									<li class="dropdown">
										<input type="hidden" class="field-relationship" name="field-relationship">
										<a href="#" class="dropdown-toggle"  onclick="return false">PULLDOWN<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li class="get" >My friend</li>
											<li class="get" >My colleague</li>
											<li class="get" >My mother</li>
											<li class="get" >My father</li>
											<li class="get" >My son</li>
											<li class="get" >My daughter</li>
											<li class="get" >My sibling</li>
											<li class="get-val"><a href="#" onclick="return false"><input type="text" class="other" placeholder="Others"/></a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
						<div class="box-add">
							<p>* We will not share their email address with anyone else.</p>
							<a class="add-fields">+ add</a>
						</div>
					</div>
				</div>
				<div class="form-footer">
					<p class="hidden">Your feedback was sent successfully. Thank you for taking time to learn about the most important thing for individual and family happiness. [Referral source] will be in touch with ways to go deeper into this material.</p>
					<div class="clone-field hidden">
						<div class="col">
							<span>first name</span>
							<input type="text" id="frist-name" class="field-item" >
						</div>
						<div class="col">
							<span>last name</span>
							<input type="text" id="last-name" class="field-item" >
						</div>
						<div class="col">
							<span>email*</span>
							<input type="email" id="email" class="field-item" >
						</div>
						<div class="col last">
							<span>relationship to you</span>
							<ul class="dropdown-relationship">
								<li class="dropdown">
									<input type="hidden" class="field-relationship" name="field-relationship">
									<a href="#" class="dropdown-toggle"  onclick="return false">PULLDOWN<b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li class="get" >My friend</li>
										<li class="get" >My colleague</li>
										<li class="get" >My mother</li>
										<li class="get" >My father</li>
										<li class="get" >My son</li>
										<li class="get" >My daughter</li>
										<li class="get" >My sibling</li>
										<li class="get-val"><a href="#" onclick="return false"><input type="text" class="other" placeholder="Others"/></a></li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="remove">
							<a class="remove-fields"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
						</div>
					</div>
					<a id="sumbut-feedback" class="btn btn-small" data-toggle="modal">SUBMIT</a>
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
