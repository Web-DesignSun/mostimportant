<?php
/*
Plugin Name: S3FlowShield
Plugin URI: http://www.s3flowshield.com/
Description: S3FlowShield Allows Secure Embedding of Media Stored on Amazon S3
Author: Rapid Crush, Inc.
Version: 3.71
Author URI: http://www.S3FlowShield.com
*/
/*
* Copyright (c) 2014, Rapid Crush, Inc.  All rights reserved.
*
* Product Version: S3FlowShield Version 3.71
*
* Contact: support@s3flowshield.com
*
* Prior to purchashing this software, you accepted it's license
* agreement.  To view a copy of the license agreement, visit:
*
* 	http://www.s3flowshield.com/license.html
*
* Note: The Flowplayer version included & distributed this software
* is released under the GNU GENERAL PUBLIC LICENSE Version 3 (GPL)
* by Flowplayer Ltd. For more information visit:
*
*		http://flowplayer.org/download/license_gpl.html
*
*/

include("lib/S3FSmain.php");
register_activation_hook(__FILE__, 's3fsActivate');
register_deactivation_hook(__FILE__, 's3fsDeactivate');
S3FS::init();
function s3fsActivate() {
	S3FS::activate();
}

function s3fsDeactivate() {
	S3FS::deactivate();
}

?>