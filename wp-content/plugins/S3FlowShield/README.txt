Contents:

	1. Additional Documentation & Training
	2. Support
	3. New Installation Instructions
	4. Upgrade from Previous Versions


******** ADDITIONAL DOCUMENTATION & TRAINING ********

Documentation is provided in the S3FlowShield Settings 
page in the WordPress Admin Console.

For additional training, including how-to videos,
please visit:

	http://www.s3flowshield.com/documentation/

	
******** SUPPORT ********

If you have questions or need support, please visit: 

	http://www.s3flowshield.com/support/
	


******** NEW INSTALLATION INSTRUCTIONS ********

Installing this software is as easy as any other WordPress Plugin

1. 	Using your FTP application, copy the S3FlowShield folder to
		the wp-content/plugins folder for your WordPress installation
		
2.	Login to you WordPress Administration Console and go to
		the "Manage Plugins" page
		
3. 	Find the S3FlowShield plugin, in the list of Inactive
		Plugins and "Activate" it

4. 	Click on S3FlowShield section at the bottom of the admin menu
		(below the Settings Menu)
		At a minimum, you will need to specify the:
		
				Registration Key
				Amazon AWS Key
				Amazon AWS Secret
				S3 Bucket Name

		Note: Your registration code is unique and can be found in
					the members portal on the "download" page
					http://www.s3flowshield.com/members/
			
		Note: You MUST provide a valid registration key or the plug
					will not function.
						

******** UPGRADE FROM PREVIOUS VERSIONs ********


1. 	Unzip the S3FlowShield zip file

2. 	Using your favorite FTP Application, open the plugins 
		directory (wp-content/plugins/) on your server and upload the 
		'S3FlowShield" folder from the unzipped file.  When asked 
		about overwriting the existing files, please say yes so 
		that the new files can be copied.

3. 	Deactivate the Plugin

4. 	Activate the Plugin
		
		
