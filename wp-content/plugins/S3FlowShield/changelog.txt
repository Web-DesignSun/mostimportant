Version 3.66 (2013-03-18)
    - Fixes: allow multiple overlay videos to work on single page
Version 3.67 (2013-05-29)
    - Improvement: avoid js concatenation issues in admin
Version 3.68 (2014-05-28)
    - Fixes: Updates for Wordpress 3.9
Version 3.69 (2014-07-08)
    - Fixes: Better compatibility with newer jQuery versions
Version 3.70 (2014-09-09)
    - Fixes: correct sizes for video overlay when different sized videos on same page
Version 3.71 (2014-09-25)
    - Fixes: better compatibility between different video player libraries