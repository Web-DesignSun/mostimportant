var HighlightDialog = {
    local_ed : 'ed',
    init : function(ed) {
        HighlightDialog.local_ed = ed;
        //tinyMCEPopup.resizeToInnerSize();
    },
    insert : function insertHighlightSection(ed) {
 
        // Try and remove existing style / blockquote 
        // tinyMCEPopup.execCommand('mceRemoveNode', false, null);

        // get the passed value from input field
        // you can create multiple fields this way and get their values
	var s3fsAuthURL = jQuery('#highlight-dialog input[name=s3fsAuthURL]:checked').val();
	var s3fsAuthURLDefault = jQuery('#highlight-dialog input#s3fsAuthURLDefault').val();
	if (s3fsAuthURL) {
		s3fsAuthURLDefault = s3fsAuthURL;
	}
	if (s3fsAuthURLDefault == 'false') {
		var file_url = jQuery('#highlight-dialog input#file_url').val();	
	} else {
		var file_url = S3FSParseURL(jQuery('#highlight-dialog input#file_url').val());
	}
	var filetype = jQuery('#highlight-dialog input[name=filetype]:checked').val();
	var s3fsAutoPlay = jQuery('#highlight-dialog input[name=s3fsAutoPlay]:checked').val();
	var s3fsAddtoSitemap = jQuery('#highlight-dialog input[name=s3fsAddtoSitemap]:checked').val();
	var s3fsPlayerVolume = jQuery('#highlight-dialog select[name=s3fsPlayerVolume]').val();
	var s3fsPlayerVolumeAudio = jQuery('#highlight-dialog select[name=s3fsPlayerVolumeAudio]').val();
	var s3fsAllowFullScreenMode = jQuery('#highlight-dialog input[name=s3fsAllowFullScreenMode]:checked').val();
	var s3fsClipScaling = jQuery('#highlight-dialog input[name=s3fsClipScaling]:checked').val();
	var s3fsBufferLength = jQuery('#highlight-dialog input#s3fsBufferLength').val();
	var s3fsBackgroundImage = jQuery('#highlight-dialog input#s3fsBackgroundImage').val();
	var s3fsBackgroundImageAudio = jQuery('#highlight-dialog input#s3fsBackgroundImageAudio').val();
	var s3fsShowControlBar = jQuery('#highlight-dialog input[name=s3fsShowControlBar]:checked').val();
	var s3fsControlBarDisplayTime = jQuery('#highlight-dialog input[name=s3fsControlBarDisplayTime]:checked').val();
	var s3fsDisplayMode = jQuery('#highlight-dialog input[name=s3fsDisplayMode]:checked').val();	
	var s3fsEmbeddedVideoWidth = jQuery('#highlight-dialog input#s3fsEmbeddedVideoWidth').val();
	var s3fsEmbeddedVideoHeight = jQuery('#highlight-dialog input#s3fsEmbeddedVideoHeight').val();
	var s3fsOverlayVideoWidth = jQuery('#highlight-dialog input#s3fsOverlayVideoWidth').val();
	var s3fsOverlayVideoHeight = jQuery('#highlight-dialog input#s3fsOverlayVideoHeight').val();
	var s3fsClickToPlayMessage = jQuery('#highlight-dialog input#s3fsClickToPlayMessage').val();
	var s3fsOverlayBorder = jQuery('#highlight-dialog input#s3fsOverlayBorder').val();
	var s3fsPlayButtonText = jQuery('#highlight-dialog input#s3fsPlayButtonText').val();
	var s3fsUseCustomPlayButton = jQuery('#highlight-dialog input[name=s3fsUseCustomPlayButton]:checked').val();
	var s3fsCustomPlayButtonURL = jQuery('#highlight-dialog input#s3fsCustomPlayButtonURL').val();
	var s3fsDownloadAnchor = jQuery('#highlight-dialog input#s3fsDownloadAnchor').val();
	var s3fsURLEncode = jQuery('#highlight-dialog input[name=s3fsURLEncode]:checked').val();
	
	
	/// File Type
	var fily_type = '';
	
	if (filetype) {
		fily_type = filetype;
	} else { /// If nothing is selected try to determine the file type
		if(file_url.match(".mov") || file_url.match(".mp4") || file_url.match(".flv") )
		{
			fily_type = 'S3VIDEO';
		} 
		else if (file_url.match(".mp3") || file_url.match(".wav") || file_url.match(".aif") )
		{
			fily_type = 'S3AUDIO';
		} 
		else if (!file_url) 
		{
			fily_type = 'S3VIDEO';
		} 
		else
		{
			fily_type = 'S3FILE';
		} 
	}
	
	/// File URL
	if (file_url)
	{ 
		file_url=' file=\''+file_url+'\'';
	} else {
		file_url=' file=\'PUT_YOUR_FILE_URL_HERE\''; 
	}
	
	/// Auth URL
	if (s3fsAuthURL=='true') { s3fsAuthURL=' authurl=\'true\''; } 
	else if (s3fsAuthURL=='false') { s3fsAuthURL=' authurl=\'false\''; }
	else { s3fsAuthURL=''; }

	/// URL Encode for S3URL
	if (s3fsURLEncode=='true') { s3fsURLEncode=' urlencode=\'true\''; } 
	else if (s3fsURLEncode=='false') { s3fsURLEncode=' urlencode=\'false\''; }
	else { s3fsURLEncode=''; }

	/// Auto Play
	if (s3fsAutoPlay=='true') { s3fsAutoPlay=' autoplay=\'true\''; } 
	else if (s3fsAutoPlay=='false') { s3fsAutoPlay=' autoplay=\'false\''; }
	else { s3fsAutoPlay=''; }

	/// Add to Video Sitemap
	if (s3fsAddtoSitemap=='true') { s3fsAddtoSitemap=' sitemap=\'true\''; } 
	else if (s3fsAddtoSitemap=='false') { s3fsAddtoSitemap=' sitemap=\'false\''; }
	else { s3fsAddtoSitemap=''; }

	
	/// Default Volume 
	if (s3fsPlayerVolume) { s3fsPlayerVolume=' vol=\'' + s3fsPlayerVolume + '\''; } else { s3fsPlayerVolume=''; } 
	
	/// Default Volume Audio
	if (s3fsPlayerVolumeAudio) { s3fsPlayerVolumeAudio=' vol=\'' + s3fsPlayerVolumeAudio + '\''; } else { s3fsPlayerVolumeAudio=''; } 
	
	/// Allow Fullscreen 
	if (s3fsAllowFullScreenMode=='true') { s3fsAllowFullScreenMode=' allowfullscreen=\'true\''; } 
	else if (s3fsAllowFullScreenMode=='false') { s3fsAllowFullScreenMode=' allowfullscreen=\'false\''; }
	else { s3fsAllowFullScreenMode=''; }
	
	/// Aspect Ratio 
	if (s3fsClipScaling=='fit') { s3fsClipScaling=' scaling=\'preserve\''; } 
	else if (s3fsClipScaling=='scale') { s3fsClipScaling=' scaling=\'fill\''; }
	else { s3fsClipScaling=''; }
	
	/// Buffer
	if (s3fsBufferLength) { s3fsBufferLength =' buffer=\'' + s3fsBufferLength + '\''; }
	else { s3fsBufferLength=''; } 
	
	/// Background Image
	if(s3fsBackgroundImage) { s3fsBackgroundImage =' bgimage=\'' + s3fsBackgroundImage + '\''; } 
	else { s3fsBackgroundImage=''; } 
	
	/// Background Image Audio
	if(s3fsBackgroundImageAudio) { s3fsBackgroundImageAudio =' bgimage=\'' + s3fsBackgroundImageAudio + '\''; } 
	else { s3fsBackgroundImageAudio=''; } 
	
	/// Video Control Bar
	if(s3fsShowControlBar == 'always') { s3fsShowControlBar =' controlbar=\'true\''; }
	else if(s3fsShowControlBar == 'autohide') { s3fsShowControlBar =' controlbar=\'autohide\''; }
	else if(s3fsShowControlBar == 'never') { s3fsShowControlBar =' controlbar=\'never\''; }
	else { s3fsShowControlBar = ''; }
	
	/// Display Remaining Time
	if (s3fsControlBarDisplayTime=='true') { s3fsControlBarDisplayTime=' time=\'true\''; } 
	else if (s3fsControlBarDisplayTime=='false') { s3fsControlBarDisplayTime=' time=\'false\''; }
	else { s3fsControlBarDisplayTime=''; }
	
	/// Display Mode
	if(s3fsDisplayMode == 'overlay') { s3fsDisplayMode =' displaymode=\'overlay\''; }
	else if(s3fsDisplayMode == 'embedded') { s3fsDisplayMode =' displaymode=\'embedded\''; }
	else { s3fsDisplayMode=''; }
	
	/// Embedded Video Width
	if(s3fsEmbeddedVideoWidth) { s3fsEmbeddedVideoWidth = ' width=\''+ s3fsEmbeddedVideoWidth + '\''; }
	else { s3fsEmbeddedVideoWidth = ''; }
	
	/// Embedded Video Height
	if(s3fsEmbeddedVideoHeight) { s3fsEmbeddedVideoHeight = ' height=\''+ s3fsEmbeddedVideoHeight + '\''; }
	else { s3fsEmbeddedVideoHeight = ''; }
	
	/// Overlay  Video Width
	if(s3fsOverlayVideoWidth) { s3fsOverlayVideoWidth = ' ovwidth=\''+ s3fsOverlayVideoWidth + '\''; }
	else { s3fsOverlayVideoWidth = ''; }
	
	/// Overlay  Video Height
	if(s3fsOverlayVideoHeight) { s3fsOverlayVideoHeight = ' ovheight=\''+ s3fsOverlayVideoHeight + '\''; }
	else { s3fsOverlayVideoHeight = '';  }
	
	/// Click to Play
	if(s3fsClickToPlayMessage) { s3fsClickToPlayMessage = ' clicktoplay=\''+ s3fsClickToPlayMessage + '\''; }
	else { s3fsClickToPlayMessage = '';  }
	
	/// Overlay Border Image
	if(s3fsOverlayBorder) { s3fsOverlayBorder = ' ovborder=\''+ s3fsOverlayBorder + '\''; }
	else { s3fsOverlayBorder = ''; }
	
	/// Play Button Text
	if(s3fsPlayButtonText) { s3fsPlayButtonText =' pbtext=\'' + s3fsPlayButtonText + '\''; } 
	else { s3fsPlayButtonText=''; } 
	
	/// Use Custom Play Button
	if (s3fsUseCustomPlayButton=='true') { s3fsUseCustomPlayButton=' custompb=\'true\''; } 
	else if (s3fsUseCustomPlayButton=='false') { s3fsUseCustomPlayButton=' custompb=\'false\''; }
	else { s3fsUseCustomPlayButton=''; }
	
	/// Play Button Image URL
	if(s3fsCustomPlayButtonURL) { s3fsCustomPlayButtonURL =' pbimg=\'' + s3fsCustomPlayButtonURL + '\''; } 
	else { s3fsCustomPlayButtonURL=''; } 
	
	/// Download Anchor
	if(s3fsDownloadAnchor) { s3fsDownloadAnchor =' anchor=\'' + s3fsDownloadAnchor + '\''; } 
	else { s3fsDownloadAnchor=''; } 
	
	
	
    var final_output = '';
	/// add the value from the input field
	
	final_output += '['+ fily_type + file_url + s3fsAuthURL + s3fsAutoPlay + s3fsAddtoSitemap + s3fsPlayerVolume + s3fsPlayerVolumeAudio + s3fsAllowFullScreenMode + s3fsClipScaling + s3fsBufferLength + s3fsBackgroundImage + s3fsBackgroundImageAudio + s3fsShowControlBar + s3fsControlBarDisplayTime + s3fsDisplayMode + s3fsEmbeddedVideoWidth + s3fsEmbeddedVideoHeight + s3fsOverlayVideoWidth + s3fsOverlayVideoHeight + s3fsClickToPlayMessage + s3fsOverlayBorder + s3fsPlayButtonText + s3fsUseCustomPlayButton + s3fsCustomPlayButtonURL + s3fsDownloadAnchor + s3fsURLEncode + ']';
	

	/// add the content of whatever text the used has had selected before clicking the button 
	final_output += '<p>'+HighlightDialog.local_ed.selection.getContent()+'</p>';
 	
 	/// add the output to the WYSWYG editor
	tinyMCEPopup.execCommand('mceReplaceContent', false, final_output);  
	tinyMCEPopup.close();
    }
};

	function S3FSParseURL(url) {
		
		var version = '0.2';
		var target_host = "s3.amazonaws.com";
		var target_file = false;
		
		url = url.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1");//trim
		
		//If we're left with empty string, return false
		if(!url || url.length < 1) {
			return false;
		}
		
		var first_char = url.charAt(0);
		var a =  document.createElement('a');
	    a.href = url;
	    var path = a.pathname.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1");
	    var segs = path.replace(/^\//,'').split('/');
	    var host = a.hostname;
	    var in_there = host.indexOf(target_host);
	    
	    //no file, return false
	    if(!path) {
			return false;
		}
		
	    if (host == target_host) {
	    	//1st case: bucketless domain, strip bucket path element
	    	if(segs.length < 1) {
	    		return false;
	    	} else {
	    		segs.splice(0,1);
	    	}
	    	
	    	if(segs.length < 1) {
				return false;
			} else {
		    	target_file = segs.join('/');
				return target_file; 
			}  
	    	
	    } else if(in_there != -1) {
	    	//2nd case: amazon url with extra bits (presume bucket), leave path as-is
	    	if(segs.length < 1) {
				return false;
			} else {
		    	target_file = segs.join('/');
				return target_file; 
			}  
			
	    } else {
	    	//3rd case: no amazon domain provided, should be relative url
	    	if("/" != first_char && "h" != first_char) {
				//no domain and no absolute root path, have to just return what we've got
	    		return url;
			} else {
				//we have a root path or domain, so return the parsed/joined url segments
				if(segs.length < 1) {
					return false;
				} else {
			    	target_file = segs.join('/');
					return target_file; 
				}  
			}
	    }
	}

tinyMCEPopup.onInit.add(HighlightDialog.init, HighlightDialog);



document.write('<base href="'+tinymce.baseURL+'" />');
