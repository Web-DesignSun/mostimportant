(function() {
    tinymce.create('tinymce.plugins.s3fs_plugin', {
        init : function(ed, url) {
  
		ed.addCommand('s3fs_button_cmd', function() {
			ed.windowManager.open({
				file : url + '/s3fs_win.php',
				width : 690 + parseInt(ed.getLang('highlight.delta_width', 0)),
				height : 387 + parseInt(ed.getLang('highlight.delta_height', 0)),
				inline : 1
			}, {
				plugin_url : url
			});
		}); 
		ed.addButton('s3fs_button', {title : 'S3FlowShield Controls', cmd : 's3fs_button_cmd', image: url + '/s3flowshield.png' });
		
	}, // End Init:
 	
        getInfo : function() {
            return {
                longname : 'S3FlowShield',
                author : 'S3FlowShield',
                authorurl : 'http://s3flowshield.com',
                infourl : 'http://s3flowshield.com',
                version : tinymce.majorVersion + "." + tinymce.minorVersion
            };
        }
    });

    tinymce.PluginManager.add('s3fs_hook', tinymce.plugins.s3fs_plugin);
})();




